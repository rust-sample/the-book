fn main() {
    println!("Hello, world!");
    another_function();

    let sum = get_sum(5, 6);
    println!("The sum is {}", sum);
}

fn another_function() {
    println!("Another function.");
}

fn get_sum(x: i32, y: i32) -> i32 {
    let sum = x + y;
    sum
}
