fn main() {
    // INTEGERS
    let a: u8 = 255;

    // let a: u8 = a + 1; will cause overflow
    println!("Number is {}", a);

    // FLOATS
    let x = 2.0; // f64
    let y: f32 = 3.0; // f32
    println!("Floats are {} and {}", x, y);

    let sum = 5 + 10;
    let difference = 95.5 - 4.3;
    let product = 4 * 30;
    let quotient = 56.7 / 32.2;
    let remainder = 43 % 5;
    println!(
        "Calculations: sum {}, difference {}, product {}, quotient {}, remainder {}",
        sum, difference, product, quotient, remainder
    );

    //  BOOLEAN
    let t = true;
    let f: bool = false; // with explicit type annotation
    println!("Booleans are {} and {}", t, f);

    // CHARS
    let c = 'z';
    let z = 'ℤ';
    let heart_eyed_cat = '😻';
    println!("Chars are {}, {} and {}", c, z, heart_eyed_cat);

    // TUPLES
    let x: (i32, f64, u8) = (500, 6.4, 1);
    let first = x.0;
    let (a, _b, _c) = x;
    println!("Values can be got as {} and {}", first, a);

    // ARRAYS
    let _a: [i32; 5] = [3, 3, 3, 3, 3];
    let a = [3; 5];
    let index = 2;
    let element = a[index];
    println!("The value of element is: {}", element);
}
